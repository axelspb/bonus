<?php
namespace app\controllers;
use Yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use app\models\Promo;

class ApiController extends Controller
{
    public function actionGet_discount_info($name)
    {
        return new ActiveDataProvider([
            'query' => Promo::find()->where('name=:name AND active=1', ['name' => $name]),
        ]);
    }

    public function actionActivate_discount($name, $city_id)
    {
        $model = Promo::find()->where('name=:name AND city_id=:city_id AND active=1', ['name' => $name, 'city_id' => $city_id])->one();
        if($model) {
            $model->active = 0;
            $model->save();
            return ['result' => true, 'payment' => $model->payment];
        }
        return ['result' => false];
    }

}