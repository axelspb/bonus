<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Promo;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Промокоды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить промокод', ['update'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin();
$ar = Yii::$app -> request -> get('Promo');
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'date_start',
                'filter' => DatePicker::widget(
                    [
                        'name' => 'Promo[date_start]',
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => isset($ar['date_start']) ? $ar['date_start'] : '',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]),
            ],
            [
                'attribute' => 'date_end',
                'filter' => DatePicker::widget(
                    [
                        'name' => 'Promo[date_end]',
                        'type' => DatePicker::TYPE_INPUT,
                        'value' => isset($ar['date_end']) ? $ar['date_end'] : '',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]),
            ],
            'payment',
            [
               'attribute' => 'city_id',
                'value' => function (Promo $data) {
                    return $data->city->name;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\City::find()->orderBy(['name'=>SORT_ASC])->all(), 'id', 'name'),
            ],
             'name',
            [
                'attribute' => 'active',
                'filter' => Promo::getStatus(),
                'value' => function (Promo $data) {
                    return Promo::getStatus($data->active);
                },
             ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
