<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use app\models\Promo;
/* @var $this yii\web\View */
/* @var $model app\models\Promo */

$this->title =  $model->isNewRecord ?  'Добавить промокод' : 'Изменить';
$this->params['breadcrumbs'][] = ['label' => 'Промокоды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$disabled = $model->active == 0 && !$model->isNewRecord ? true:false;
?>
<div class="promo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="promo-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'date_start')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Выберите дату начала.'],
            'disabled' => $disabled,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
            ]
        ]);?>

        <?= $form->field($model, 'date_end')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'Выберите дату окончания.'],
            'disabled' => $disabled,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
            ]
        ]) ?>

        <?= $form->field($model, 'payment')->textInput(['disabled' => $disabled,]) ?>

        <?= $form->field($model, 'city_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\City::find()->orderBy(['name'=>SORT_ASC])->all(), 'id', 'name'),['disabled' => $disabled,]) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true,'disabled' => $disabled,]) ?>

        <?php if(!$model->isNewRecord):?>
            <?= $form->field($model, 'active')->widget(\kartik\switchinput\SwitchInput::className(), [
                'type' => \kartik\switchinput\SwitchInput::CHECKBOX,
                'pluginOptions' => [
                    'onText' => Promo::getStatus(0),
                    'offText' => Promo::getStatus(1),
                ]
            ]) ?>
        <?php endif;?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
