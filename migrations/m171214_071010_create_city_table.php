<?php

use yii\db\Migration;

/**
 * Handles the creation of table `city`.
 */
class m171214_071010_create_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

    }
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%city}}');
    }
}
