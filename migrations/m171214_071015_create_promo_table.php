<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promo`.
 */
class m171214_071015_create_promo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%promo}}', [
            'id'            => $this->primaryKey(),
            'date_start'    => $this->date()->notNull(),
            'date_end'      => $this->date()->notNull(),
            'payment'       => $this->integer()->notNull()->unsigned(),
            'city_id'       => $this->integer()->notNull(),
            'name'          => $this->string(50)->unique(),
            'active'        => $this->smallInteger(1)->defaultValue(1)->unsigned(),
        ]);
        $this->addForeignKey('city_id_fk', '{{%promo}}', 'city_id', '{{%city}}', 'id', 'CASCADE');
        $this->createIndex('date_start_inx', '{{%promo}}', 'date_start');
        $this->createIndex('date_end_inx', '{{%promo}}', 'date_end');
        $this->createIndex('active_inx', '{{%promo}}', 'active');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('active_inx', '{{%promo}}');
        $this->dropIndex('date_start_inx', '{{%promo}}');
        $this->dropIndex('date_end_inx', '{{%promo}}');
        $this->dropForeignKey('city_id_fk', '{{%promo}}');
        $this->dropTable('{{%promo}}');
    }
}
