<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "promo".
 *
 * @property integer $id
 * @property string $date_start
 * @property string $date_end
 * @property integer $payment
 * @property integer $city_id
 * @property string $name
 *
 * @property City $city
 */
class Promo extends \yii\db\ActiveRecord
{
    const SCENARIO_SEARCH = 'search';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_start', 'date_end', 'payment', 'city_id'], 'required', 'except' => self::SCENARIO_SEARCH],
            [['date_start', 'date_end'], 'date', 'format' => 'Y-mm-d'],
            ['date_start', function ($attribute, $params) {
                if(!is_null($this->date_start) && !is_null($this->date_end)) {
                    if(strtotime($this->date_end)<strtotime($this->date_start)) {
                        $this->addError('date_start', 'Дата начала должна быть меньше или равна даты окончания.');
                    }
                }
            }, 'except' => self::SCENARIO_SEARCH],
            [[ 'city_id','active'], 'integer'],
            [['payment'], 'integer', 'min' => 1],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique', 'except' => self::SCENARIO_SEARCH],
            [['name'], 'match', 'pattern' => "/^[a-zA-Z]+$/"],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    public static function getStatus($status = null)
    {
        $statuses = [
            0 => 'Не активен',
            1 => 'Активен'
        ];
        if(!is_null($status)) return $statuses[$status];
        return $statuses;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'active' => 'Статус',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'payment' => 'Вознаграждение, руб.',
            'city_id' => 'Тарифная зона',
            'name' => 'Код (латиница)',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['id'], $fields['name']);
        $fields['city'] = function () {
            return $this -> city -> name;
        };
        return $fields;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function search($params)
    {
        $query = Promo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_start' => $this->date_start,
            'active' => $this->active,
            'date_end' => $this->date_end,
            'payment' => $this->payment,
            'city_id' => $this->city_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}
